# [sociolog.org](https://sociolog.org) source codes

<br/>

### Run sociolog.org on localhost

    # vi /etc/systemd/system/sociolog.org.service

Insert code from sociolog.org.service

    # systemctl enable sociolog.org.service
    # systemctl start sociolog.org.service
    # systemctl status sociolog.org.service

http://localhost:4058
